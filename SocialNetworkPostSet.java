import java.util.List;
import java.util.ArrayList;

public class SocialNetworkPostSet {
	protected String postId;
	protected ArrayList<String> repostIdList;
	protected int totalFollowers;
	
	public SocialNetworkPostSet(String pId, ArrayList<String> rpIdList, int total) {
		this.postId = pId;
		this.repostIdList = rpIdList;
		this.totalFollowers = total;
	}
	
	public String getPostId() {
		return this.postId;
	}
	
	public void setPostId(String pId) {
		this.postId = pId;
	}
	
	public ArrayList<String> getRepostIdList(){
		return this.repostIdList;
	}
	
	public void setRepostIdList(ArrayList<String> list){
		this.repostIdList = list;
	}
	
	public int getTotalFollowers(){
		return this.totalFollowers;
	}
	
	public void setTotalFollowers(int total){
		this.totalFollowers = total;
	}
}