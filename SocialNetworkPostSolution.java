import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.util.Optional;

public class SocialNetworkPostSolution {
	// Assume the input list is ordered by post time (means no repost before the original post)
	// Assume the postIds are all unique 
	public void socialNetworkAnalysis(BufferedReader reader) {
		ArrayList<SocialNetworkPostSet> res = new ArrayList<SocialNetworkPostSet>();		
		
		try {
			//skip the first line
			String line = reader.readLine();
			
			while((line = reader.readLine()) != null) {
				String[] parsedLine = line.replaceAll("\\s", "").split(",");
				if(parsedLine.length == 3) {					
					// add original post
					if(parsedLine[1].equals("-1")) {
						SocialNetworkPostSet newOriginalPost = new SocialNetworkPostSet(parsedLine[0], new ArrayList<String>(Arrays.asList(parsedLine[0])), Integer.parseInt(parsedLine[2]));
						res.add(newOriginalPost);
					} 
					// add/update repost and followers count						
					else {
						processRepost(res, parsedLine[1], parsedLine[0], Integer.parseInt(parsedLine[2]));
					}
				}
			}
			reader.close();
		} catch (IOException ex){
			System.err.println(ex.toString());
		}
		
		for(SocialNetworkPostSet postSet : res) {
			System.out.println(postSet.getPostId() + ": " + Integer.toString(postSet.getTotalFollowers()));
		}
	}
	
	private void processRepost(List<SocialNetworkPostSet> list, String originalId, String repostId, int followers) {		
		Optional<SocialNetworkPostSet> element = list.stream().filter(p -> p.getRepostIdList().contains(originalId)).findFirst();
		if(element.isPresent()) {			
			SocialNetworkPostSet thisPostSet = element.get();			
			thisPostSet.getRepostIdList().addAll(new ArrayList<String>(Arrays.asList(repostId)));
			thisPostSet.setTotalFollowers(thisPostSet.getTotalFollowers() + followers);
		}
	}
}