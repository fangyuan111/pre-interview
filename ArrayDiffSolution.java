import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.util.Optional;

public class ArrayDiffSolution {
// Assume the input arrays may not be sorted
	public void arrayDiff(BufferedReader reader) {
		String current = null, target = null;
		try {
			current = reader.readLine();
			target = reader.readLine();
			reader.close();
		} catch (IOException ex) {
			System.err.println(ex.toString());
		}
		
		try{
			List<Integer> currentArr = Arrays.stream(current.replaceAll("\\s", "").split(",")).map(s -> Integer.valueOf(s)).collect(Collectors.toList());
			List<Integer> targetArr = Arrays.stream(target.replaceAll("\\s", "").split(",")).map(s -> Integer.valueOf(s)).collect(Collectors.toList());
			
			Map<Integer, Integer> countingMap = new HashMap<>();
					
			// O(n)
			for(Integer currentItem : currentArr) {
				if(countingMap.get(currentItem) == null) {
					countingMap.put(currentItem, 1);
				} else {
					countingMap.put(currentItem, countingMap.get(currentItem) + 1);
				}			
			}
			System.out.println("");
			// O(m)
			System.out.print("additions: [ ");
			for(Integer targetItem : targetArr) {
				// takes care of duplicate items as well
				if(countingMap.get(targetItem) != null && countingMap.get(targetItem) >= 1){
					countingMap.put(targetItem, countingMap.get(targetItem) - 1);					
				} else {
					// print adddtions:
					System.out.print(targetItem.toString() + " ");
				}
			}
			System.out.println("]");
			
			// O(n)
			System.out.print("deletions: [ ");			
			for(Integer currentItem : currentArr) {
				if(countingMap.get(currentItem) != null && countingMap.get(currentItem) > 0) {
					System.out.print(currentItem.toString() + " ");
					countingMap.put(currentItem, countingMap.get(currentItem) - 1);
				}
			}
			System.out.println("]");
		} catch (Exception ex) {
			System.err.println(ex.toString());
		}
	}
}