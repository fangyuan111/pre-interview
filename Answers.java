import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.util.Optional;

public class Answers {
	// First argument: question name; Second argument: input file.
	public static void main (String[] args) {
		if(args.length < 2) {
			System.err.println("Invalid arguments count!");
		} 
		
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(args[1]));
		} catch (FileNotFoundException ex) {
			System.err.println(ex.toString());
		}
		
		ArrayDiffSolution arrayDiff = new ArrayDiffSolution();
		SocialNetworkPostSolution social = new SocialNetworkPostSolution();
		
		String questionName = args[0];
		if(questionName.equals("array") || questionName.equals("1")) {
			arrayDiff.arrayDiff(reader);			
		} else if(questionName.equals("social") || questionName.equals("2")) {
			social.socialNetworkAnalysis(reader);
		} else {
			System.err.println("Invalid question name: " + questionName);
		}	
	}
}