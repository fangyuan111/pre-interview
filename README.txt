Instructions:
	1. Install Java 1.8
	2. Setup environment variable
	3. Download the code and cmd to the folder
	4. The 'Answers.java' file contains the main function
	5. The 'ArrayDiffSolution.java' file is for the Array Diff problem
	6. The 'SocialNetworkPostSolution.java' and 'SocialNetworkPostSet.java file' files are for the Social Network Post problem

To Compile, run the following command:
	javac Answers.java SocialNetworkPostSet.java ArrayDiffSolution.java SocialNetworkPostSolution.class
	or
	javac *.java


To run:
	1. Array Diff
		java -cp . Answers array array_diff_input.txt
		or
		java -cp . Answers 1 array_diff_input.txt

	2. Social Network Post
		java -cp . Answers social social_network_analysis_input.csv
		or
		java -cp . Answers 2 social_network_analysis_input.csv